const express = require('express')
const app = express();

const session = require('express-session');
const passport = require('passport')
const localStrategy = require('passport-local').Strategy

const path = require('path');
const server = require('http').Server(app); 
const fs = require('fs');

const { socketConnection,sendMessage } = require('./utility/io');

const cors = require('cors'); 
const bodyParser = require('body-parser');
const port = 4000;
const constanta = require('./utility/const')

// setup my socket server

app.use(cors({
  "origin": "*",
  "methods": "GET,HEAD,PUT,PATCH,POST,DELETE",
  "preflightContinue": false,
  "optionsSuccessStatus": 204
}))

//allowed request data format
app.use(bodyParser.urlencoded({limit: '50mb'}));
app.use(bodyParser.json({limit: '50mb'}));

// Configurasi middleware
app.use(session({
  resave: false,
  saveUninitialized: true,
  secret: 't@1k0ch3ng',
  name: 'secretName',
  cookie: {
      sameSite: true,
      maxAge: 60000
  },
}))

app.use(passport.initialize()) 
app.use(passport.session())    

authUser = (user, password, done) => {
  console.log(`Value of "User" in authUser function ----> ${user}`)         //passport will populate, user = req.body.username
  console.log(`Value of "Password" in authUser function ----> ${password}`) //passport will popuplate, password = req.body.password
  let authenticated_user = { id: 123, name: "Kyle"} 
  return done (null, authenticated_user ) 
}

passport.use(new localStrategy (authUser))

passport.serializeUser( (user, done) => { 
  console.log(`--------> Serialize User`)
  console.log(user)     
  done(null, user.id)
})

passport.deserializeUser((id, done) => {
  console.log("---------> Deserialize Id")
  console.log(id)
  done (null, {name: "Kyle", id: 123} )      
}) 

//ejs part
app.engine('.ejs', require('ejs').__express);
app.set('views', path.join(__dirname, '/views'));
app.set('view engine', 'ejs');

app.get("/login", (req, res) => {
  res.sendFile(path.join(__dirname+'/public/frontend/index.html'));
})

app.post ("/login", passport.authenticate('local', {
  successRedirect: "/dashboard",
  failureRedirect: "/login",
}))


//routes
//static page
app.use(express.static('public'));

app.use("/assets", express.static(__dirname + "/public/frontend/assets"));

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname+'/public/frontend/index.html'));
});

//modules:
app.get('/dashboard', (req, res) => {
  res.render('main', {page:'dashboard'});
});
app.get('/categories', (req, res) => {
  res.render('main', {page:'categories'});
});

app.get('/products', (req, res) => {
  res.render('main', {page:'products'});
});


//  REST API
app.get('/api/v1/health',(req,res)=>{
  res.status(200).json({
    "status"  : true,
    "message" : "Ok"
  })
})
const userRouter = require('./routes/users')
app.use('/api/v1/users',cors(),userRouter)

const categoriesRouter = require('./routes/categories');
app.use('/api/v1/categories',categoriesRouter);

const productsRouter = require('./routes/products');
app.use('/api/v1/products',productsRouter);


const ordersRouter = require('./routes/orders');
app.use('/api/v1/orders',ordersRouter);
/*
const ordersItemRouter = require('./routes/orders_item');
app.use('/api/v1/ordersItem',ordersItemRouter);

*/
server.listen(port, () => {
  console.log(`app running at http://localhost:${port}`)
})

socketConnection(server)