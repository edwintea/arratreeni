const con = require('../config/database')
const {validationResult} = require('express-validator')
const WK = require('../utility/const')
const { sendMessage } = require('../utility/io');
const query=require('../repository/model')

const table="products_category"

module.exports={
    get:async(req,res)=>{
        try{
            await query.execQuery(req,res,"SELECT * FROM "+table+" ORDER BY id DESC");
        }catch(e){
            return res.status(404).json({
                status:false,
                message:"Notfound",
                data:[]
            })
        }
        
    },
    save:(req,res)=>{
        const error = validationResult(req)
        if(!error.isEmpty()){
            return res.status(422).json({
                errors:error.array()
            })
        }

        let formData={
            category                  :   req.body.category,
            description               :   req.body.description
        }

        con.query("INSERT INTO "+table+" SET ? ",formData,function(err,rows){
            if(err){
                console.log(err)
                return res.status(500).json({
                    status: false,
                    message:err.message
                })
            }else{
                //just for test
                //sendMessage('client', {name:'',message:'save workflows Ok'});

                return res.status(201).json({
                    status:true,
                    message:"Data saved succesfully",
                    data:req.body
                })
            }
        })
        
    },
    delete:(req,res)=>{
        const error = validationResult(req)
        if(!error.isEmpty()){
            return res.status(422).json({
                errors:error.array()
            })
        }

        con.query("DELETE FROM "+table+" WHERE id='"+req.body.id+"' ",function(err,rows){
            if(err){
                return res.status(500).json({
                    status: false,
                    message: err.message
                })
            }else{
                return res.status(200).json({
                    status:true,
                    message:"Delete Successfull",
                    data:req.body
                })
            }
        })
    }
}
