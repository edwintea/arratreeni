/*
+-------------+--------------+------+-----+-------------------+-------------------+
| Field       | Type         | Null | Key | Default           | Extra             |
+-------------+--------------+------+-----+-------------------+-------------------+
| id          | int          | NO   | PRI | NULL              | auto_increment    |
| id_category | int          | YES  |     | NULL              |                   |
| title       | varchar(250) | YES  |     | NULL              |                   |
| description | text         | YES  |     | NULL              |                   |
| price       | decimal(8,2) | YES  |     | NULL              |                   |
| image       | text         | YES  |     | NULL              |                   |
| createdAt   | datetime     | YES  |     | CURRENT_TIMESTAMP | DEFAULT_GENERATED |
| updatedAt   | datetime     | YES  |     | CURRENT_TIMESTAMP | DEFAULT_GENERATED |
+-------------+--------------+------+-----+-------------------+-------------------+
*/
const express = require('express')
const router = express.Router()
const con = require('../config/database')
const {body} = require('express-validator')
const controller= require('../controller/productsController')


router.get('/all',controller.get)

//post
router.post('/save',[
    body('id_category').notEmpty(),
    body('title').notEmpty(),
    body('description').notEmpty(),
    body('price').notEmpty(),  
    body('image').notEmpty()
],controller.save)


//delete
router.delete('/delete',[
    body('id').notEmpty()
],controller.delete)


module.exports=router

