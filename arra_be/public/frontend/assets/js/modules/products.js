/////////////////////// Products ////////////////////////////
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah').attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

let Products={
    params:    {
        "id"                  : "",
        "id_category"         : "",
        "title"               : "",
        "description"         : "",
        "price"               : 0,
        "image"               : ""
    },
    getAllCategory    :   function(e){
        Ajax(Api.Categories.GetAll.URL,Api.Categories.GetAll.Method,"",function(){
            e()
        })
    },
    showAll    :   function(e){
        Ajax(Api.Products.GetAll.URL,Api.Products.GetAll.Method,"",function(){
            e()
        })
    },
    delete    :   function(e){
        Ajax(Api.Products.Delete.URL,Api.Products.Delete.Method,Products.params,function(){
            e()
        })
    },
    default:()=>{
        //default
        $('input').val('');
        $('.btnAddProducts').show()
        $('.Products_data').show()

        Products.showAll(function(){
            render('.products_data',Products.viewData(),()=>{
                Products.handler()
            })
        })
    },
  
    viewData   :   (type)=>{
        el="";
            if(res.data.length > 0){
                $.each(res.data,function(i,e){
                    
                    el+="<div class='col-xs-12 col-sm-6 col-md-4'>";
                        el+="<div class='image-flip' >";
                            el+="<div class='mainflip flip-0'>";
                                el+="<div class='frontside'>";
                                    el+="<div class='card'>";
                                        el+="<div class='card-body text-center'>";
                                            el+="<p><img  src='"+e.image+"' alt='card image'></p>";
                                            el+="<h4 class='card-title'> Product : "+e.title+"</h4>";
                                            el+="<p class='card-text'>Category : "+e.category+"</p>";
                                            el+="<p class='card-text'>Price : $"+numberWithCommas(e.price)+"</p>";
                                            el+="<p class='card-text' >Description :<span style='text-align:justify;'>"+e.description+"</span></p>";
                                            el+="<a href='#' class='btn btn-primary btn-sm'><i class='fa fa-plus'></i></a>";
                                        el+="</div>";
                                    el+="</div>";
                                el+="</div>";
                                el+="<div class='backside'>";
                                    el+="<div class='card'>";
                                        el+="<div class='card-body text-center mt-4'>";
                                            el+="<h4 class='card-title'> Product : "+e.title+"</h4>";
                                            el+="<p class='card-text'>Category : "+e.category+"</p>";
                                            el+="<p class='card-text'>Price : $"+numberWithCommas(e.price)+"</p>";
                                            el+="<p class='card-text' >Description :<span style='text-align:justify;'>"+e.description+"</span></p>";
                                            el+="<a href='#' class='btn btn-sm btnDelete' data-id='"+e.id+"' style='color:white;'><span class='fas fa-trash'></span> Delete</a>";
                                        el+="</div>";
                                    el+="</div>";
                                el+="</div>";
                            el+="</div>";
                        el+="</div>";
                    el+="</div>";
                })
            }else{
                el+="<div class='col-md-12' >";
                    el+="<div class='alert alert-warning'>Empty Records</div>";
                el+="</div>";
            }
        return el;
    },
    handler:()=>{
        $('.nav-link').each(function(i,e){
            $(e).off().on('click',function(){
                let show = $(e).data('show');
                localStorage.setItem('tab',show)
            })
        })

        $('.btnDelete').each(function(i,e){
            $(e).off().on('click',function(){

                confirm("Are you sure want to delete Product's Category ?",()=>{
                    Products.params.id=$(e).data('id');
                    Products.delete(function(){
                        notify(res.message,"info")
                        Products.default();
                    });
                })
                
                return false
            })

        })

        $('.btnAddProducts').off().on('click',function(){
            $(this).hide();
            $('.products_data').hide();
            $('.products_form').show();

            Products.getAllCategory(function(){
                el="";
                el+="<option value=''>-- Select Category --</option>";
                if(res.data.length > 0){
                    
                    $.each(res.data,function(i,e){
                        el+="<option value='"+e.id+"'>"+e.category+"</option>";
                    })
                    $('#optCategory').empty().html(el);
                }
            })
            
            
            tinymce.init({
                selector:'#description',
                setup: function (editor) {
                    editor.on('keyup', function (e) {
                        console.debug('Editor contents was modified. Contents: ' + editor.getContent({
                            format: 'text'
                        }));
                    });
                }
            });
        })

        $('.btnSave').off().on('click',function(){
            var elmForm = $("#myForm");
            if(elmForm){
                elmForm.validator('validate');
                var elmErr = elmForm.find('.has-error');
                if(elmErr && elmErr.length > 0){
                    return false;
                }else{
                    confirm("Are you sure want to create Product's ?",()=>{
                        //SET DATA
                        Products.params.id_category=$('#optCategory').find(':selected').val()
                        Products.params.title=$('#txtTile').val();
                        Products.params.price=$('#txtPrice').val();
                        Products.params.image=$('#blah').attr('src');

                        tinymce.triggerSave();
                        Products.params.description=$('#description').val()
                
                        Ajax(Api.Products.Save.URL,"POST",Products.params,function(){
                            notify(res.message,"info")
                            $('.btnCancel').trigger('click');
                            Products.default()
                        })
                        
                    })
                    
                }
            }

            return false;
        })

        $('.btnCancel').off().on('click',function(){
            $('.btnAddProducts').show();
            $('.products_data').show();
            $('.products_form').hide();
        })
    },
    init    :  ()=>{
        notify("You build your Products of Products here...","info")
        Products.default();
        Products.handler();
    }
}