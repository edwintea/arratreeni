const con = require('../config/database')
const {validationResult} = require('express-validator')
const WK = require('../utility/const')
const { sendMessage } = require('../utility/io');
const query=require('../repository/model')
const table="orders"
const uuidv6= require('uuid');

module.exports={
    get:async(req,res)=>{
        try{
            await query.execQuery(req,res,"SELECT * FROM "+table+"  ORDER BY id DESC");
        }catch(e){
            return res.status(404).json({
                status:false,
                message:"Notfound",
                data:[]
            })
        }
        
    },
    save:(req,res)=>{
        const error = validationResult(req)
        if(!error.isEmpty()){
            return res.status(422).json({
                errors:error.array()
            })
        }

        let order_no=uuidv6();

        let formData={
            order_no        :   order_no,
            address         :   req.body.address,
            total           :   req.body.total,
            createdBy       :   req.body.createdBy
        }

        con.query("INSERT INTO "+table+" SET ? ",formData,function(err,rows){
            if(err){
                console.log(err)
                return res.status(500).json({
                    status: false,
                    message:err.message
                })
            }else{
                let formData1={
                    order_no        :   order_no,
                    id_product      :   req.body.orders.id_product,
                    price           :   req.body.orders.price
                }
        
                con.query("INSERT INTO orders_item SET ? ",formData1,function(err,rows){
                    if(err){
                        console.log(err)
                        return res.status(500).json({
                            status: false,
                            message:err.message
                        })
                    }else{
                        //just for test
                        sendMessage('client', {name:'new_orders',message:'save orders Ok'});
        
                        return res.status(201).json({
                            status:true,
                            message:"Data saved succesfully",
                            data:req.body
                        })
                    }
                })
            }
        })
        
    },
    delete:(req,res)=>{
        const error = validationResult(req)
        if(!error.isEmpty()){
            return res.status(422).json({
                errors:error.array()
            })
        }

        con.query("DELETE FROM "+table+" WHERE id='"+req.body.id+"' ",function(err,rows){
            if(err){
                return res.status(500).json({
                    status: false,
                    message: err.message
                })
            }else{
                return res.status(200).json({
                    status:true,
                    message:"Delete Successfull",
                    data:req.body
                })
            }
        })
    }
}
