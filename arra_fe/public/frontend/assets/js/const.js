const BASE_API      =   HOST_API+"api/v1/"
const BASE_PAGE     =   HOST
const DASHBOARD_URL       =   HOST+"ecommerce/"
const MESSAGE       =   {
    LOGIN_SUCCESS   :   " Active in Octopuce.io at this moment",
    WORKFLOWS_SAVE  :   " Create Workflow at this moment",
    WORKFLOWS_DELETE:   " Delete Workflow at this moment",
    WORKFLOWS_EDIT  :   " Edit Workflow at this moment",
    DB_ERROR        :   " Something Wrong in DB Connection"

}
