/////////////////////// CATEGORIES ////////////////////////////

let Categories={
    params:    {
        "id"                  : "",
        "category"            : ""
    },
    showAll    :   function(e){
        Ajax(Api.Categories.GetAll.URL,Api.Categories.GetAll.Method,"",function(){
            e()
        })
    },
    delete    :   function(e){
        Ajax(Api.Categories.Delete.URL,Api.Categories.Delete.Method,Categories.params,function(){
            e()
        })
    },
    default:()=>{
        //default
        $('input').val('');
        $('.btnAddCategories').show()
        $('.categories_data').show()

        Categories.showAll(function(){
            render('.categories_data',Categories.viewData(),()=>{
                Categories.handler()
            })
        })
    },
    viewData   :   ()=>{
        el="";
            if(res.data.length > 0){
                let no=1;
                var el="";
                el+="<table class='table table-striped'>";
                el+="<thead>";
                    el+="<td style='width:10px;'>Action</td>";
                    el+="<td>No</td>";
                    el+="<td>Category</td>";
                    el+="<td>Description</td>";
                el+="</thead>"
                el+="</tbody>"
                $.each(res.data,function(i,e){
                    el+="<tr>";
                        el+="<td><a href='#' class='btnDelete' data-id='"+e.id+"'><span class='fas fa-trash'></span></a></td>";
                        el+="<td>"+no+"</td>";
                        el+="<td>"+e.category+"</td>";
                        el+="<td>"+e.description+"</td>";

                    el+="</tr>";
                    no++;
                })
                el+="</tbody>";
                el+="</table>";
            }else{
                el+="<div class='col-md-12' >";
                    el+="<div class='alert alert-warning'>Empty Records</div>";
                el+="</div>";
            }
        return el;
    },
    handler:()=>{
        $('.nav-link').each(function(i,e){
            $(e).off().on('click',function(){
                let show = $(e).data('show');
                localStorage.setItem('tab',show)
            })
        })

        $('.btnDelete').each(function(i,e){
            $(e).off().on('click',function(){

                confirm("Are you sure want to delete Product's Category ?",()=>{
                    Categories.params.id=$(e).data('id');
                    Categories.delete(function(){
                        notify(res.message,"info")
                        Categories.default();
                    });
                })
                
                return false
            })

        })

        $('.btnAddCategories').off().on('click',function(){
            $(this).hide();
            $('#txtCategoryName').focus();
            $('.categories_data').hide();
            $('.categories_form').show();
            
            
            tinymce.init({
                selector:'#description',
                setup: function (editor) {
                    editor.on('keyup', function (e) {
                        console.debug('Editor contents was modified. Contents: ' + editor.getContent({
                            format: 'text'
                        }));
                    });
                }
            });

            
        })

        $('.btnSave').off().on('click',function(){
            var elmForm = $("#myForm");
            if(elmForm){
                elmForm.validator('validate');
                var elmErr = elmForm.find('.has-error');
                if(elmErr && elmErr.length > 0){
                    return false;
                }else{
                    confirm("Are you sure want to create Product's Category ?",()=>{
                        //SET DATA
                        Categories.params.category=$('#txtCategoryName').val()
                        tinymce.triggerSave();
                        Categories.params.description=$('#description').val()
                
                        Ajax(Api.Categories.Save.URL,"POST",Categories.params,function(){
                            notify(res.message,"info")
                            $('.btnCancel').trigger('click');
                            Categories.default()
                        })
                        
                    })
                    
                }
            }

            return false;
        })

        $('.btnCancel').off().on('click',function(){
            $('.btnAddCategories').show();
            $('.categories_data').show();
            $('.categories_form').hide();
        })
    },
    init    :  ()=>{
        notify("You build your Categories of Products here...","info")
        Categories.default();
        Categories.handler();
    }
}