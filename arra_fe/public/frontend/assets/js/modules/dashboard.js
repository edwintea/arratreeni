$(function(){
    var Dashboard={
        handler:function(){
            $.each($('.btnDetail'),function(i,e){
                $(e).off().on('click',function(){
                    let id = $(e).data('id');
                    Products.showOrderItems(id,function(){
                        let detail="Order No : "+res.data[0].order_no+"<br>"
                        detail+="Product : "+res.data[0].title+"<br>"
                        detail+="Category : "+res.data[0].category+"<br>"
                        detail+="Price : $"+res.data[0].price+"<br>"

                        alert(detail);

                    })
                    
                    return false;
                })
            })
        },
        init:function(){
            Products.init();
            $('#mdlOrders').off().on('click',function(){
                Products.showOrders(localStorage.getItem('id'),function(){
                    $('#modalViewOrders').modal('show')

                    var el="";
                    if(res.data.length > 0){
                        let no=1;
                        var el="";
                        el+="<table class='table table-striped'>";
                        el+="<thead>";
                            el+="<td style='width:10px;'>Action</td>";
                            el+="<td>No</td>";
                            el+="<td>Order No</td>";
                            el+="<td>Address</td>";
                            el+="<td>Phone</td>";
                            el+="<td>Date</td>";
                        el+="</thead>"
                        el+="</tbody>"
                        $.each(res.data,function(i,e){
                            el+="<tr>";
                                el+="<td><a href='#' class='btnDetail' data-id='"+e.order_no+"'>View</a></td>";
                                el+="<td>"+no+"</td>";
                                el+="<td>"+e.order_no+"</td>";
                                el+="<td>"+e.address+"</td>";
                                el+="<td>"+e.phone+"</td>";
                                el+="<td>"+e.updatedAt+"</td>";
        
                            el+="</tr>";
                            no++;
                        })
                        el+="</tbody>";
                        el+="</table>";
                    }else{
                        el+="<div class='col-md-12' >";
                            el+="<div class='alert alert-warning'>Empty Records</div>";
                        el+="</div>";
                    }

                    $('#modalContent').empty().html(el);
                    Dashboard.handler()
                })
                
                return false;
            })
        }
    }

    Dashboard.init()

})