/////////////////////// Products ////////////////////////////
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah').attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

let Products={
    params:    {
        "id"                  : "",
        "id_category"         : "",
        "title"               : "",
        "description"         : "",
        "price"               : 0,
        "image"               : "",
        "orders"              :{},
        "createdBy"           : ""
    },
    getAllCategory    :   function(e){
        Ajax(Api.Categories.GetAll.URL,Api.Categories.GetAll.Method,"",function(){
            e()
        })
    },
    showAll    :   function(e){
        Ajax(Api.Products.GetAll.URL,Api.Products.GetAll.Method,"",function(){
            e()
        })
    },
    order    :   function(e){
        Ajax(Api.Products.Order.URL,Api.Products.Order.Method,Products.params,function(){
            e()
        })
    },
    showOrders    :   function(id,e){
        Ajax(Api.Orders.GetMyOrder(id),"GET","",function(){
            e()
        })
    },
    showOrderItems    :   function(id,e){
        Ajax(Api.Orders.GetOrderItem(id),"GET","",function(){
            e()
        })
    },
    default:()=>{
        //default
        $('input').val('');
        $('.btnAddProducts').show()
        $('.Products_data').show()

        Products.showAll(function(){
            render('.products_data',Products.viewData(),()=>{
                Products.handler()
            })
        })
    },
  
    viewData   :   (type)=>{
        el="";
            if(res.data.length > 0){
                $.each(res.data,function(i,e){
                    
                    el+="<div class='col-md-4' style='margin-top:0.5%;'>";
                        el+="<div class='card p-3'>";
                            el+="<div class='d-flex flex-row mb-3'><img src='"+e.image+"' width='70'>";
                                el+="<div class='d-flex flex-column ml-2'><span>"+e.category+"</span><span class='text-black-50'>"+e.title+"</span><span class='ratings'><i class='fa fa-star'></i><i class='fa fa-star'></i><i class='fa fa-star'></i><i class='fa fa-star'></i></span> $"+numberWithCommas(e.price)+"</div>";
                            el+="</div>";
                            el+="<h6>"+e.description+"</h6>";
                            el+="<div class='d-flex justify-content-between install mt-3'><span>Sold 0 times</span><span class='text-primary'><a href='#' class='btnOrder' data-id='"+e.id+"' data-price='"+e.price+"'>Order Now&nbsp;</a><i class='fa fa-angle-right'></i></span></div>";
                        el+="</div>";
                    el+="</div>";
                })
            }else{
                el+="<div class='col-md-12' >";
                    el+="<div class='alert alert-warning'>Empty Records</div>";
                el+="</div>";
            }
        return el;
    },
    handler:()=>{
       
        $('.btnOrder').each(function(i,e){
            $(e).off().on('click',function(){

                confirm("Are you sure want to order This Product ?",()=>{
                    //SET DATA
                    
                    Products.params.address=localStorage.getItem("address");
                    Products.params.phone=localStorage.getItem("phone");
                    Products.params.total=$(e).data('price');
                    Products.params.createdBy=localStorage.getItem('id');
                    Products.params.orders.id_product=$(e).data('id');
                    Products.params.orders.price=$(e).data('price');
                    
                    Products.order(function(){
                        //notify(res.message,"info")
                        
                    });
                })
                
                return false
            })

        })


    },
    init    :  ()=>{
        Products.default();
        Products.handler();
    }
}