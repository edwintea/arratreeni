
const Api={
    Users:  {
        Login       :   {
            URL     :    BASE_API+"users/login",
            Method  :   "POST"
        },
        register       :   {
            URL     :    BASE_API+"users/save",
            Method  :   "POST"
        },
        Forgot       :   {
            URL     :    BASE_API+"users/forgot",
            Method  :   "POST"
        }
    },
    Categories     :  {
        GetAll         :   {
            URL     :    BASE_API+"categories/",
            Method  :   "GET"
        },
        GetActive         :   {
            URL     :    BASE_API+"categories/active",
            Method  :   "GET"
        },
        Save        :   {
            URL     :    BASE_API+"categories/save",
            Method  :   "POST"
        },
        Delete      :  {
            URL     :    BASE_API+"categories/delete",
            Method  :   "DELETE"
        }
    },
    Products     :  {
        GetAll         :   {
            URL     :    BASE_API+"products/all",
            Method  :   "GET"
        },
        GetActive         :   {
            URL     :    BASE_API+"products/active",
            Method  :   "GET"
        },
        Save        :   {
            URL     :    BASE_API+"products/save",
            Method  :   "POST"
        },
        Order      :   {
            URL     :    BASE_API+"orders/save",
            Method  :   "POST"
        }
    },
    Orders     :  {
        GetAll         :   {
            URL     :    BASE_API+"orders/all",
            Method  :   "GET"
        },
        GetMyOrder         :   function(id){
            return BASE_API+"orders/"+id;
        },
        GetOrderItem         :   function(id){
            return BASE_API+"orders/items/"+id;
        },
        GetActive         :   {
            URL     :    BASE_API+"orders/active",
            Method  :   "GET"
        },
        Save        :   {
            URL     :    BASE_API+"orders/save",
            Method  :   "POST"
        },
        Delete      :   {
            URL     :    BASE_API+"orders/delete",
            Method  :   "DELETE"
        }
    },
    OrdersItem     :  {
        GetAll         :   {
            URL     :    BASE_API+"ordersItem/all",
            Method  :   "GET"
        },
        GetActive         :   {
            URL     :    BASE_API+"ordersItem/active",
            Method  :   "GET"
        },
        Save        :   {
            URL     :    BASE_API+"ordersItem/save",
            Method  :   "POST"
        },
        Delete      :   {
            URL     :    BASE_API+"ordersItem/delete",
            Method  :   "DELETE"
        }
    },
    
    Page  : (obj)=>{
        return HOST+obj;
     }
}

const Component={
    'Render'    :   (url,type,e)=>{
        $.ajax({
            headers     : {
                'Accept'        : 'application/json',
                'Content-Type'  : 'application/json'
            },
            xhrFields   :  'withCredentials:true',
            url         :   url,
            crossOrigin :   true,
            type        :   type,
            data        :   {},
            dataType    :   "html",
            success: function(data) {
                console.log(data)
                res=data;
                e();
            },
            error: function(data) {
                console.log(data)
                if(data.status==500){
                    showAlert('#contentConnectionLists','warning','Empty Record.')
                }
                
                e();
            },
            
        })

    }
}

var res=[];
var req={};

function clearRequest(){
    req={}
}

let Ajax=function(url,type,datas,e){
    res=[];
    $.ajax({
        headers     : {
            'Accept'        : 'application/json',
            'Content-Type'  : 'application/json'
        },
        xhrFields   :  'withCredentials:true',
        url         :   url,
        crossOrigin :   true,
        type        :   type,
        data        :   JSON.stringify(datas),
        dataType    :   "json",
        success: function(data) {
            res=data;
            e();
        },
        error: function(data) {
            res=data.responseJSON;
            console.log(res)
            notify(res.message,"error")
        },
        
    })
}