/*
+-----------+--------------+------+-----+-------------------+-------------------+
| Field     | Type         | Null | Key | Default           | Extra             |
+-----------+--------------+------+-----+-------------------+-------------------+
| id        | int          | NO   | PRI | NULL              | auto_increment    |
| category  | varchar(255) | YES  |     | NULL              |                   |
| createdAt | datetime     | YES  |     | CURRENT_TIMESTAMP | DEFAULT_GENERATED |
| updatedAt | datetime     | YES  |     | CURRENT_TIMESTAMP | DEFAULT_GENERATED |
+-----------+--------------+------+-----+-------------------+-------------------+
*/
const express = require('express')
const router = express.Router()
const con = require('../config/database')
const {body} = require('express-validator')
const controller= require('../controller/categoriesController')


router.get('/',controller.get)

//post
router.post('/save',[
    body('category').notEmpty()
],controller.save)


//delete
router.delete('/delete',[
    body('id').notEmpty()
],controller.delete)


module.exports=router

