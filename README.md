SYSTEM SPECIFICATION:

Front End : 
- Stack :
	- Bootstrap ,css ,scss
	- Nodejs with Express
		node version : v16.20.2
		npm version : 8.19.4
	- port : 5000
- Socket.io for realtime data update from BackEnd (New Products)


Back End : 
- Stack :
	- Bootstrap ,css ,scss
	- Nodejs with Express
		node version : v16.20.2
		npm version : 8.19.4
	- port : 4000
	- Database : MySQL
- Socket.io for realtime data update from BackEnd push to Front End (New Products)

How To Running the Program :

pull from repository:
git pull : https://gitlab.com/edwintea/arratreeni.git

1. Import MySQL Database :
	- import ara.sql file to Mysql Server (in SQL folder)
2. run Backend :
	- open terminal , then enter to arra_be folder.
	- run command : node index.js
	- application will run on localhost:4000
	
3. run Frontend :
	- open terminal , then enter to arra_be folder.
	- run command : node index.js
	- application will run on localhost:4000